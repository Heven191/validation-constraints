package net.tncy.fva.validator;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ISBNValidatorTest
{
    @Test
    public void isValidCode()
    {
        ISBNValidator v = new ISBNValidator();
        String isbn = "978-3-16-148410-0";
        assertTrue( v.isValid(isbn, null) );
    }

    @Test
    public void isIllegalCharacterCode()
    {
        ISBNValidator v = new ISBNValidator();
        String isbn = "978-3-16-1/8410-0";;
        assertFalse( v.isValid(isbn, null) );
    }

    @Test
    public void isInvalidCode()
    {
        ISBNValidator v = new ISBNValidator();
        String isbn = "978-3-16-148410-0";
        assertTrue( v.isValid(isbn, null) );
        isbn = "978-3-16-148710-0";
        assertFalse( v.isValid(isbn, null) );
    }

    @Test
    public void isEmptyCodeInvalidCode()
    {
        ISBNValidator v = new ISBNValidator();
        String isbn = "";
        assertFalse( v.isValid(isbn, null) );
    }

    @Test
    public void is13ISBN()
    {
        ISBNValidator v = new ISBNValidator();
        String isbn = "978-3-16-148410-0";
        assertTrue( v.isValid(isbn, null) );
        assertFalse( v.isValid("978-3-16-110-0",null));
    }


}
