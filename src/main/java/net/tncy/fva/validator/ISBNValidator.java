package net.tncy.fva.validator;


import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    @Override
    public void initialize(ISBN constraintAnnotation) {
        // Ici le validateur peut accéder aux attribut de l’annotation.
    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        if (bookNumber.equals("")){
            return false;
        }
        String[] numbers=bookNumber.split("-");
        String temp ="";
        for (int i=0;i<numbers.length;i++){
            temp=temp+numbers[i];
        }
        if (temp.length()!=13){
            return false;
        }
        try {
            int sum=0;
            for (int j = 0; j < temp.length(); j++) {
                if ((j+1)%2==0){
                    sum+=3*Integer.parseInt(String.valueOf(temp.charAt(j)));
                }else{
                    sum+=Integer.parseInt(String.valueOf(temp.charAt(j)));
                }
            }
            if (sum%10!=0){
                return false;
            }
        }
        catch(Exception e){
            return false;
        }

        return true;
    }
}